package com.yeyouluo.mybatis.druid.dao;

import java.util.List;

import com.yeyouluo.mybatis.druid.pojo.User;

public interface IUserDao {
	public List<User> findAllUsers();
}

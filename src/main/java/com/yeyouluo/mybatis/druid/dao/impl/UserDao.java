package com.yeyouluo.mybatis.druid.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.yeyouluo.mybatis.druid.dao.IUserDao;
import com.yeyouluo.mybatis.druid.pojo.User;
import com.yeyouluo.mybatis.druid.utils.MyBatisUtil;

public class UserDao implements IUserDao {

	private static final String namespace = "com.yeyouluo.mybatis.druid.dao.userDao.";
	
	@Override
	public List<User> findAllUsers() {
		List<User> users = null;
		SqlSession session = MyBatisUtil.getSession();
		try {
			users = session.selectList(namespace + "findAllUsers", User.class);
			//注意：此处有陷阱，如果做了更新、插入或删除操作，必须使用：
			//session.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			MyBatisUtil.closeSession(session);
		}
		return users;
	}

}
